const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const mysql = require('mysql')

const syntaxQuery = require('./syntaxQuery');
const configDb = {
  host: '',
  user: '',
  password: '',
  database: ''
}

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
  console.log('connect')

  socket.on('disconnect', function() {
    console.log('disconnect')
  })

  // subscribe
  socket.on('subscribe', (res) => {
    try {
      if (res.room) {
        socket.join(res.room);
        console.log(`subscribe in room succesfully in ${res.room}`)
        if (res.companyId) {
          // Define our root db
          let rootDB = mysql.createConnection({
            host: configDb.host,
            user: configDb.user,
            password: configDb.password,
            database: configDb.database
          })
          let store = null
          rootDB.query(`SELECT mysql_database FROM pos_company WHERE id_company='${res.companyId}'`)
            .on('result', function (data) {
              store = Buffer.from(data.mysql_database).toString('base64');
            })
            .on('end', function () {
              io.to(res.room).emit('setNode', store);
            })

          // rootDB.end();
        }
      }
    } catch(e) {
      console.log(e)
    }
  });

  // unsubscribe
  socket.on('unsubscribe', (room) => {
    try {
      socket.leave(room);
      console.log(`unsubscribe in room succesfully in ${room}`)
    } catch(e) {
      console.log(e)
    }
  });

  var query = function (database, sql, params) {
    return new Promise((resolve, reject) => {
      database.query(sql, params, (err, rows) => {
        if (err) {
          return reject(err);
        }
        resolve(rows);
      });
    });
  }

  var closeConnection = function (database) {
    return new Promise((resolve, reject) => {
      database.end(err => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });
  }

  var getNode = (database) => {
    let base64data = database.toString('base64');
    let buffOne = Buffer.from(base64data, 'base64').toString('ascii');
    let index = buffOne.indexOf("_");
    let node = buffOne.substring(index+1);
    let buffTwo = Buffer.from(node, 'base64').toString('ascii');

    let tenantDB = mysql.createConnection({
      host: configDb.host,
      user: configDb.user,
      password: configDb.password,
      database: buffTwo
    })

    return tenantDB
  }

  // get item by id
  socket.on('getItemById', (res) => {
    try {
      if (res.node) {
        var tenantDB = getNode(res.node)

        var data = null
        let queryQty = `SELECT quantity FROM pos_item_outlet WHERE id_item='${res.id_item}'
        AND id_outlet='${res.id_outlet}'`;

        let queryItems = `SELECT pos_items.id_item, pos_items.id_category, item_name, item_price, pos_items.is_active, is_package, sku_number AS sku, barcode, is_manual_price, category_name, stock_control FROM pos_items LEFT JOIN pos_item_categories cat ON pos_items.id_category=cat.id_category WHERE pos_items.deleted_at IS NULL AND id_item='${res.id_item}'`

        let queryItemPackages = `SELECT id_item FROM pos_item_packages WHERE id_package='${res.id_item}'`
        let queryPackageItemCategories = `SELECT id_item FROM pos_item_packages WHERE id_package='${res.id_item}'`

        query(tenantDB, queryItems)
          .then(rows => {
            data = rows;
            return query(tenantDB, queryQty);
          })
          .then(qty => {
            data[0]['stock_item'] = qty[0].quantity
            return query(tenantDB, queryItemPackages);
          })
          .then(packageItems => {
            data[0]['package_items'] = packageItems.length ? packageItems : [];
            return query(tenantDB, queryPackageItemCategories);
          })
          .then(packageCategories => {
            let queryPromotions = `SELECT * FROM pos_promotions LEFT JOIN pos_items ON pos_promotions.reward_product_id=pos_items.id_item WHERE EXISTS (SELECT * FROM pos_outlets INNER JOIN pos_promotions_outlets ON pos_outlets.id_outlet = pos_promotions_outlets.outlet_id WHERE pos_promotions.id = pos_promotions_outlets.promotion_id AND status = 1 AND (outlet_id = '${res.id_outlet}' AND need_code = 0 AND (start_date <= CURDATE() AND end_date >= CURDATE() OR (start_date IS NULL))) AND pos_outlets.deleted_at IS NULL) AND EXISTS (SELECT * FROM pos_promotion_req_items WHERE pos_promotions.id = pos_promotion_req_items.promotion_id AND (requirement_product_id='${res.id_item}' OR requirement_product_id = '${data[0].id_category}')) AND pos_promotions.deleted_at IS NULL ORDER BY pos_promotions.created_at DESC LIMIT 1`

            data[0]['package_categories'] = packageCategories.length ? packageCategories : [];
            return query(tenantDB, queryPromotions);
          })
          .then(promotions => {
            data[0]['promotions'] = promotions.length ? promotions : [];
            return closeConnection(tenantDB);
          })
          .then(() => {
            io.to(res.room).emit('sendItemById', data[0]);
          });

        console.log('send notification succesfully', res.room)
      }
    } catch(e) {
      console.log(e)
    }
  })

  // send account config
  socket.on('sendAccountConfig', (res) => {
    io.to(res.room).emit('updateAccountConfig', res.data);
  });

  // send tax sudah
  socket.on('getTaxConfig', (res) => {
    let tenantDB = getNode(res.node)
    let params = {
      id_outlet: res.id_outlet,
      tenantDB: tenantDB
    }

    let data = []
    query(tenantDB, syntaxQuery.queryTax(params))
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateTaxConfig', data);
        }
      });
  });

  // send pin setting sudah
  socket.on('getPinSettingConfig', (res) => {
    let tenantDB = getNode(res.node)
    let data = []
    query(tenantDB, syntaxQuery.queryPinSettings())
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updatePinSettingConfig', data);
        }
      });
  });

  // send payment methods setting sudah
  socket.on('getPaymentMethodsConfig', (res) => {
    let tenantDB = getNode(res.node)

    let data = []
    query(tenantDB, syntaxQuery.queryPaymentMethods())
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updatePaymentMethodsConfig', data);
        }
      });
  });

  // send nominal_cash setting sudah
  socket.on('getNominalCashConfig', (res) => {
    let tenantDB = getNode(res.node)
    let data = []
    query(tenantDB, syntaxQuery.queryNominalCash())
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateNominalCashConfig', data);
        }
      });
  });

  // send vouchers setting sudah
  socket.on('getVouchersConfig', (res) => {
    let tenantDB = getNode(res.node)
    let data = []
    query(tenantDB, syntaxQuery.queryVoucher())
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateVouchersConfig', data);
        }
      });
  });

  // send refund reasons setting sudah
  socket.on('getRefundReasonsConfig', (res) => {
    let tenantDB = getNode(res.node)
    let data = []
    query(tenantDB, syntaxQuery.queryRefundReasons())
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateRefundReasonsConfig', data);
        }
      });
  });

  // send attribute_receipts setting sudah
  socket.on('getAttributeReceiptsConfig', (res) => {
    let tenantDB = getNode(res.node)
    let data = []
    query(tenantDB, syntaxQuery.queryAttributeReceipts({id_outlet: res.id_outlet}))
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateAttributeReceiptsConfig', data[0]);
        }
      });
  });

   // send promotions setting
   socket.on('getPromotionsConfig', (res) => {
    let tenantDB = getNode(res.node)
    let data = []
    query(tenantDB, syntaxQuery.queryPromotions({id_outlet: res.id_outlet}))
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updatePromotionsConfig', data);
        }
      });
  });

  // send user_has_pin setting
  socket.on('getUserHasPinConfig', (res) => {
    let rootDB = mysql.createConnection({
      host: configDb.host,
      user: configDb.user,
      password: configDb.password,
      database: configDb.database
    })

    let params = {
      id_outlet: res.id_outlet,
      id_company: res.id_company,
      rootDB: configDb.database
    }

    let data = []
    query(rootDB, syntaxQuery.queryUserHasPin(params))
      .then(rows => {
        data = rows;
        return closeConnection(rootDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateUserHasPinConfig', data[0]);
        }
      });
  });

  // send list_user setting
  socket.on('getListUser', (res) => {
    let tenantDB = getNode(res.node)
    let params = {
      id_outlet: res.id_outlet,
      rootDB: configDb.database
    }

    let data = []
    query(tenantDB, syntaxQuery.queryListUser(params))
      .then(rows => {
        data = rows;
        return closeConnection(tenantDB);
      })
      .then(() => {
        if (data.length) {
          io.to(res.room).emit('updateListUserConfig', data);
        }
      });
  });

});

http.listen(3000, function() {
  console.log('listening on *:3000');
});
