module.exports = {
  queryTransaction: function (params) {
    let query = `SELECT max_transaction, current_transaction, max_customer, current_customers, max_offline, sale_with_pic, stock_minus, partial_payment, taxable, extra_item, auto_shift, discount_overall, default_paper FROM ${params.rootDB}.pos_subscriptions CROSS JOIN (SELECT count(*) as current_transaction FROM ${params.tenantDB}.pos_sales_transactions WHERE deleted_at IS NULL) as trans CROSS JOIN (SELECT count(*) as current_customers FROM ${params.tenantDB}.pos_customers WHERE deleted_at IS NULL) as customers CROSS JOIN (SELECT * FROM pos_dgti.pos_company WHERE id_company='${params.id_company}') as company WHERE company_id='${params.id_company}'`;

    return query
  },
  /*tax*/
  queryTax: (params) => {
    let query = `SELECT id_tax, tax_name, percentage, is_include, is_default FROM pos_taxes WHERE id_outlet='${params.id_outlet}' AND is_active=1 AND is_default=1 AND deleted_at is null`;

    return query
  },
  /*pin_settings*/
  queryPinSettings: (params) => {
    let query = `SELECT name, locked, description FROM pos_pin_permissions WHERE status=1`;

    return query
  },
  /*user_has_pin*/
  queryUserHasPin: (params) => {
    let query = `SELECT id as user_id, name as user_name, user_pin FROM ${params.rootDB}.pos_users WHERE id_company='${params.id_company
    }' AND user_pin IS NOT NULL AND user_pin != ''`;

    return query
  },
  /*payment_methods*/
  queryPaymentMethods: (params) => {
    let query = `SELECT id, name, type, description, show_caption, caption, chargeable, charge_value FROM pos_payment_methods WHERE deleted_at IS NULL`;

    return query
  },
  /*nominal_cash*/
  queryNominalCash: (params) => {
    let query = `SELECT nominal FROM pos_nominal_cashs WHERE deleted_at IS NULL`;

    return query
  },
  /*vouchers*/
  queryVoucher: (params) => {
    let query = `SELECT id, promo_id, code, disposable, start, end, is_percent, discount_value, description FROM pos_promo_details WHERE start <= curdate() AND end >= curdate() AND is_used=0 AND deleted_at IS NULL`;

    return query
  },
  /*promotions*/
  queryPromotions: (params) => {
    let query = `SELECT * FROM pos_promotions WHERE EXISTS (SELECT * FROM pos_outlets INNER JOIN pos_promotions_outlets ON pos_outlets.id_outlet = pos_promotions_outlets.outlet_id WHERE pos_promotions.id = pos_promotions_outlets.promotion_id AND status = 1 AND (pos_promotions_outlets.outlet_id = '${params.id_outlet}' AND (start_date <= curdate() AND end_date >= curdate() OR (start_date IS NULL))) AND pos_outlets.deleted_at IS NULL) AND pos_promotions.deleted_at IS NULL`;

    // let query = `SELECT * FROM pos_promotions INNER JOIN pos_promotions_outlets ON pos_outlets.id_outlet=pos_promotions_outlets.outlet_id WHERE pos_promotions.id=pos_promotions_outlets.promotion_id AND status=1 AND (pos_promotions_outlets.outlet_id='${params.id_outlet}' AND (start_date <= CURDATE() AND end_date >= CURDATE() OR (start_date IS NULL))) AND pos_outlets.deleted_at IS NULL) AND pos_promotions.deleted_at IS NULL`;

    return query
  },
  /*refund_reasons*/
  queryRefundReasons: (params) => {
    let query = `SELECT id, reason, need_notes FROM pos_refund_reasons`;

    return query
  },
  /*add_ons*/
  queryAddOns: (params) => {
    let query = `SELECT add_on FROM ${params.rootDB}.pos_tenant_add_ons WHERE is_active=1`;

    return query
  },
  /*attribute_receipts*/
  queryAttributeReceipts: (params) => {
    let query = `SELECT headers, footers, image_path FROM pos_setting_receipt WHERE id_outlet='${params.id_outlet}' LIMIT 1`;

    return query
  },
  /*list_user*/
  queryListUser: (params) => {
    let query = `SELECT users.id, username, name, phone FROM pos_outlet_user outlet_user JOIN ${params.rootDB}.pos_users users ON outlet_user.id_user=users.id WHERE id_outlet='${params.id_outlet}'`;

    return query
  }
};
